# -*- coding: utf-8 -*-
import csv
import datetime

import requests
from bs4 import BeautifulSoup
from loguru import logger


__author__ = "Ahmosys"
__email__ = "ahmosyspro@pm.me"
__licence__ = "MIT"


AMELIE_SEARCH_URL = "http://annuairesante.ameli.fr/recherche.html"
AMELIE_QUERY_PARAMS_SEARCH_URL = {
    "type": "ps",
    "ps_profession": "34",
    "ps_profession_label": "Médecin généraliste",
    "ps_localisation": "HERAULT (34)",
    "localisation_category": "departements",
    "submit_final": "Rechercher"    
}
AMELIE_SEARCH_HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
}
SESSION = requests.Session()
NUMBER_PAGE_TO_FETCH = 50

def get_search_page(url: str) -> requests.Response:
    """Requête de la page de recherche des professionnels de santé et renvoie la réponse de la requête.

    Returns:
        requests.Response: La réponse de la requête.
    """
    r = SESSION.post(url=url, params=AMELIE_QUERY_PARAMS_SEARCH_URL, headers=AMELIE_SEARCH_HEADERS)
    if r.status_code != 200:
        logger.warning("Une erreur est survenue lors de la requête.")
    r_sorted = SESSION.get(r.url.replace("tri-aleatoire", "tri-nom_asc"))
    if r.status_code != 200:
        logger.warning("Une erreur est survenue lors de la requête.")
    return r_sorted


def get_professional_list(req_response: requests.Response) -> list[dict]:
    """Récupère la liste des professionnels de santé et renvoie une liste de dictionnaires contenant les informations des professionnels.

    Args:
        req_response (requests.Response): La réponse de la requête.

    Returns:
        list[dict]: Une liste de dictionnaires contenant les informations des professionnels.
    """
    profesionnals_list = []
    soup = BeautifulSoup(req_response.content, "html.parser")
    profesionnal_list = soup.find_all("div", class_ = "item-professionnel")
    for profesionnal in profesionnal_list:
        professional_info = {
            "name": profesionnal.find("h2", class_ = "ignore-css").text if profesionnal.find("h2", class_ = "ignore-css") else "Non renseigné",
            "job": profesionnal.find("div", class_ = "item left specialite").text if profesionnal.find("div", class_ = "item left specialite") else "Non renseigné",
            "address": profesionnal.find("div", class_ = "item left adresse").text.strip() if profesionnal.find("div", class_ = "item left adresse") else "Non renseigné",
            "number": profesionnal.find("div", class_ ="item left tel").text.replace("\xa0", "") if profesionnal.find("div", class_ ="item left tel") else "Non renseigné"
        }
        names = [p["name"] for p in profesionnals_list]
        if professional_info["name"] not in names:
            logger.info("Ajout du professionnel de santé dans la liste.")
            profesionnals_list.append(professional_info)
    return profesionnals_list


def write_to_csv(professionals_list: list[dict]):
    """Écrit les informations des professionnels dans un fichier CSV.

    Args:
        professionals_list (list[dict]): Une liste de dictionnaires contenant les informations des professionnels.
    """
    with open(f"professionals-{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.csv", "w", newline="") as file:
        logger.info("Écriture des informations dans le fichier CSV.")
        writer = csv.DictWriter(file, fieldnames=professionals_list[0].keys())
        writer.writeheader()
        writer.writerows(professionals_list)


def get_all_professionals():
    """Récupère la liste des professionnels de santé et renvoie une liste de dictionnaires contenant les informations des professionnels.

    Returns:
        list[dict]: Une liste de dictionnaires contenant les informations des professionnels.
    """
    final_list = []
    initial_req = SESSION.post(url=AMELIE_SEARCH_URL, params=AMELIE_QUERY_PARAMS_SEARCH_URL, headers=AMELIE_SEARCH_HEADERS)
    if initial_req.status_code != 200:
        logger.warning("Une erreur est survenue lors de la requête.")
    SESSION.get(initial_req.url.replace("tri-aleatoire", "tri-nom_asc")) # Tri par ordre alphabétique sur le nom
    for i in range(1, NUMBER_PAGE_TO_FETCH + 1):
        logger.info(f"Requête de la page {str(i)}/50.")
        r = SESSION.get(f"http://annuairesante.ameli.fr/professionnels-de-sante/recherche/liste-resultats-page-{i}-par_page-20-tri-nom_asc.html")
        if r.status_code != 200:
            logger.warning("Une erreur est survenue lors de la requête.")
        final_list.extend(get_professional_list(r))
    return final_list


if __name__ == "__main__":
    # Récupère tout les professionnels de santé
    profesionnal_list = get_all_professionals()
    write_to_csv(profesionnal_list)
    # Récupère les professionnels de santé de la page 1
    res = get_search_page(AMELIE_SEARCH_URL)
    list_professionals = get_professional_list(res)
    write_to_csv(list_professionals)
